<?php

use WPDesk\ApiClient\Client\ClientFactory;

class TestClientFactory extends \PHPUnit\Framework\TestCase
{

    /**
     * Prepare client options.
     *
     * @param bool $isCachedClient
     *
     * @return \Mockery\MockInterface|\WPDesk\ApiClient\Client\ApiClientOptions
     */
    private function prepareOptions($isCachedClient = false)
    {
        $options = Mockery::mock(\WPDesk\ApiClient\Client\ApiClientOptions::class);
        $options->shouldReceive('getApiClientClass')
                ->withAnyArgs()
                ->andReturn(\WPDesk\ApiClient\Client\ClientImplementation::class);

        $options->shouldReceive('getHttpClientClass')
                ->withAnyArgs()
                ->andReturn(\WPDesk\HttpClient\Curl\CurlClient::class);

        $options->shouldReceive('getSerializerClass')
                ->withAnyArgs()
                ->andReturn(\WPDesk\ApiClient\Serializer\JsonSerializer::class);

        $options->shouldReceive('getLogger')
                ->withAnyArgs()
                ->andReturn(new \Psr\Log\NullLogger());

        $options->shouldReceive('getApiUrl')
                ->withAnyArgs()
                ->andReturn('https://app.flexibleshipping.com/api/v1');

        $options->shouldReceive('getDefaultRequestHeaders')
                ->withAnyArgs()
                ->andReturn(array());

        $options->shouldReceive('isCachedClient')
                ->withAnyArgs()
                ->andReturn($isCachedClient);

        $options->shouldReceive('getTimeout')
                ->withAnyArgs()
                ->andReturn(30);

        return $options;
    }

    /**
     * Test createClient method.
     */
    public function testCreateClient()
    {

        $clientFactory = new ClientFactory();
        $client = $clientFactory->createClient($this->prepareOptions());
        $this->assertInstanceOf(\WPDesk\ApiClient\Client\ClientImplementation::class, $client);
    }

    /**
     * Test createClient method.
     */
    public function testCreateClientCached()
    {

        $clientFactory = new ClientFactory();
        $client = $clientFactory->createClient($this->prepareOptions(true));
        $this->assertInstanceOf(\WPDesk\ApiClient\Client\CachedClient::class, $client);
    }

}