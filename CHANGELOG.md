### [1.2.3] - 2024-11-25
### Fixed
- Memory consumption in api client - logger calls consumes memory, specially when large responses are logged

## [1.2.1] - 2020-12-08
### Fixed
- default headers should overwrite build in headers

## [1.2.0] - 2019-06-12
### Fixed
- CacheResolver in cached client

## [1.1.0] - 2019-05-30
### Added
- Timeout in api client options interface
