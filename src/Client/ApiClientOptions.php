<?php

namespace WPDesk\ApiClient\Client;

use Psr\Log\LoggerInterface;
use WPDesk\ApiClient\Serializer\SerializerOptions;
use WPDesk\HttpClient\HttpClientOptions;

interface ApiClientOptions extends HttpClientOptions, SerializerOptions
{

    /**
     * @return LoggerInterface
     */
    public function getLogger();

    /**
     * @return string
     */
    public function getApiUrl();

    /**
     * @return array
     */
    public function getDefaultRequestHeaders();

    /**
     * @return bool
     */
    public function isCachedClient();

    /**
     * @return string
     */
    public function getApiClientClass();
}