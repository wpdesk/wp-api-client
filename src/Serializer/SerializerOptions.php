<?php

namespace WPDesk\ApiClient\Serializer;

interface SerializerOptions
{
    /**
     * @return string
     */
    public function getSerializerClass();
}