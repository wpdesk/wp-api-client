[![pipeline status](https://gitlab.com/wpdesk/wp-api-client/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-api-client/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-api-client/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-api-client/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-api-client/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-api-client/commits/master)

API Client
==========
